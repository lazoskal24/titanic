import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles =  ["Mr.", "Mrs.", "Miss."]
    missing_ages_list = []
    medians_list = []
    def extract_second_word(name):
        return name.split(',')[1].split()[0]

    df['SecondWord'] = df['Name'].apply(extract_second_word)

    for title in titles:
        missing_ages = df[df['SecondWord'] == title]['Age'].isna().sum()
        median = df[df['SecondWord'] == title]['Age'].median()
        df['Age'].fillna(median, inplace=False)
        missing_ages_list.append(missing_ages)
        medians_list.append(median)
    df.drop(columns=['SecondWord'], inplace=True)
    result = []
    for i in range(len(titles)):
        result.append((titles[i], int(missing_ages_list[i]), int(medians_list[i])))
    return result
